package net.customware.confluence.plugin.pagehistoryslider;

import com.atlassian.confluence.content.render.xhtml.DefaultConversionContext;
import com.atlassian.confluence.content.render.xhtml.Renderer;
import com.atlassian.confluence.core.ConfluenceActionSupport;
import com.atlassian.confluence.core.VersionHistorySummary;
import com.atlassian.confluence.pages.AbstractPage;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.pages.actions.PageAware;
import com.atlassian.confluence.velocity.htmlsafe.HtmlSafe;
import com.atlassian.sal.api.component.ComponentLocator;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;


public class ViewPageHistorySliderAction extends ConfluenceActionSupport implements PageAware
{
    private AbstractPage page;

    private PageManager pageManager;

    public AbstractPage getPage()
    {
        return page;
    }

    public void setPage(AbstractPage abstractPage)
    {
        page = abstractPage;
    }

    public boolean isPageRequired()
    {
        return true;
    }

    public boolean isLatestVersionRequired()
    {
        return true;
    }

    public boolean isViewPermissionRequired()
    {
        return true;
    }

    public void setPageManager(PageManager pageManager)
    {
        this.pageManager = pageManager;
    }

    public Collection<AbstractPage> getPreviousVersions()
    {
        List<VersionHistorySummary> versionHistorySummaries = pageManager.getVersionHistorySummaries(getPage());
        List<AbstractPage> historyPages = new ArrayList<AbstractPage>();
        

        for (VersionHistorySummary versionHistorySummary : versionHistorySummaries)
            historyPages.add(pageManager.getAbstractPage(versionHistorySummary.getId()));

        return historyPages;
    }

    @HtmlSafe
    @SuppressWarnings("unused")
    public String getEditorXhtml(AbstractPage abstractPage)
    {
        return getEditRenderer().render(
                abstractPage.getBodyAsString(),
                new DefaultConversionContext(abstractPage.toPageContext())
        );
    }

    Renderer getEditRenderer()
    {
        return ComponentLocator.getComponent(Renderer.class, "editRenderer");
    }
}
