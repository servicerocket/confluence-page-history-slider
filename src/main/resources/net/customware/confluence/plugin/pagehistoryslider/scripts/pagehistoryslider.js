
jQuery(function($) {
    var historyslider = {
        iframesLoadedCount : 0,

        getFieldset : function(historyDiv) {
            return $("fieldset.hidden", historyDiv);
        },

        getParams : function(historyDiv) {
            var params = {};

            $("input", this.getFieldset(historyDiv)).each(function() {
                if (params[this.name]) {
                    params[this.name].push(this.value);
                } else {
                    params[this.name] = [ this.value ];
                }
            });

            return params;
        },

        getPreviousVersionsCount : function(historyDiv) {
            return $("iframe.plugin_pagehistory_viewport_iframe", historyDiv).length;
        },

        getContextPath : function() {
            return $("#confluence-context-path").attr("content");
        },

        getViewPortForm : function(viewPortDiv) {
            return $("form.plugin_pagehistory_viewport_form", viewPortDiv);
        },

        getViewPortIframe : function(viewPortDiv) {
            return $("iframe.plugin_pagehistory_viewport_iframe", viewPortDiv);
        },

        getViewPortIframeContainer : function(viewPortDiv) {
            return $("div.plugin_pagehistory_viewport_iframe_container", viewPortDiv);
        },

        updateViewingVersionNote : function(historyDiv, viewPortDiv) {
            var versionViewNote = $("div.version-info", historyDiv);
            var viewPortDivFieldset = $("fieldset.hidden", viewPortDiv);

            var params = {};
            $("input", viewPortDivFieldset).each(function() {
                params[this.name] = this.value;
            });

            $("input[name='selectedPageVersions']", versionViewNote).val(params["version"]);
            $("span.plugin_pagehistory_viewing_version_note", versionViewNote).html(params["viewingVersionNote"]);
            $("div.plugin_pagehistory_viewing_version_comment", versionViewNote).html(params["versionComment"]);
        },

        getIframeBody : function(iframe) {
            var iframeElement = iframe.get(0);
            var iframeDocument = iframeElement.contentWindow || iframeElement.contentDocument;
            return $((iframeDocument.document ? iframeDocument.document : iframeDocument).body);
        },

        initViewPortIframe : function(historyDiv, viewPortDiv) {
            this.getViewPortForm(viewPortDiv).submit(function() {
                $.data(this, "submitted", true);
                historyslider.getViewPortIframeContainer(viewPortDiv).removeClass("invisible");
                return true;
            });

            this.getViewPortIframe(viewPortDiv).load(function() {
                if ($.data(historyslider.getViewPortForm(viewPortDiv).get(0), "submitted")) {
                    $.data(this, "contentLoaded", true);

                    var theIframe = $(this);
                    $("a", historyslider.getIframeBody(theIframe)).each(function() {
                        this.target = "_top";
                    });

                    historyslider.iframesLoadedCount = historyslider.iframesLoadedCount + 1;
                } else {
                    historyslider.getIframeBody($(this)).html("<img src='" + historyslider.getContextPath() + "/images/icons/wait.gif'/>");
                }
            });
        },

        initViewPortIframes : function(historyDiv) {
            $("div.plugin_pagehistory_viewport", historyDiv).each(function() {
                historyslider.initViewPortIframe(historyDiv, $(this));
            });

            setInterval(function() {
                if (historyslider.iframesLoadedCount) {
                    historyslider.getViewPortIframeContainer(historyDiv).each(function() {
                        var iFrameContainer = $(this);
                        if (!iFrameContainer.hasClass("invisible")) {
                            iFrameContainer.addClass("active");

                            var iFrame = iFrameContainer.children("iframe");
                            var iFrameBody = historyslider.getIframeBody(iFrame);
                            var contentDiv = $("div#main", iFrameBody);
                            iFrame.css({
                                height: Math.max(contentDiv[0].clientHeight, contentDiv[0].scrollHeight) + "px"
                            });
                        }
                    });
                }
            }, 500);
        },

        showVersion : function(historyDiv, historyVersionId) {
            var viewPortIframe = $("iframe[name='plugin_pagehistory_page_" + historyVersionId +"']", historyDiv);
            var viewPortIframeContainer = viewPortIframe.parent();

            if (viewPortIframeContainer.hasClass("invisible")) {
                this.hideAllViewPortIframeContainers(historyDiv);

                var viewPortDiv = viewPortIframeContainer.parent();
                if ($.data(viewPortIframe.get(0), "contentLoaded")) {
                    historyslider.updateViewingVersionNote(historyDiv, viewPortDiv);
                    viewPortIframeContainer.removeClass("invisible");
                } else {
                    historyslider.updateViewingVersionNote(historyDiv, viewPortDiv);

                    var viewPortForm = $("form.plugin_pagehistory_viewport_form", viewPortIframe.parent().parent());
                    viewPortForm.submit();
                }
            }
        },

        hideAllViewPortIframeContainers : function(historyDiv) {
            this.getViewPortIframeContainer(historyDiv).addClass("invisible").removeClass("active");
        },

        getSliderDiv : function(historyDiv) {
            return $("div.plugin_pagehistory_slider", historyDiv);
        },

        initSliderTicks : function(historyDiv, tickCount) {
            var sliderDiv = this.getSliderDiv(historyDiv);

            for (var i = 0; i < tickCount; ++i) {
                var tickDiv = $(document.createElement("div"));

                tickDiv.addClass("plugin_pagehistoryslider_tick").css({
                    left : (100 / tickCount * i) + "%",
                    width : (100 / tickCount) + "%"
                }).appendTo(sliderDiv);
            }

            $("div.plugin_pagehistoryslider_tick:first", sliderDiv).css("border-left", "none");
        },

        getPageDiffDiv : function(diffHtml) {
            return $("div#page-diffs", $(document.createElement("div")).html(diffHtml));
        },

        getAbsoluteDiffVersionSpans : function(historyDiv) {
            return $(".plugin_pagehistory_absolute_diff_version", historyDiv);
        },

        getVersionPriorTo : function(historyDiv, version) {
            var versions = this.getParams(historyDiv)["version"];
            var versionsCount = historyslider.getPreviousVersionsCount(historyDiv);
            var versionPrior = null;

            $.each(versions, function(index) {
                if (this == version && index < versionsCount - 1) {
                    versionPrior = versions[index + 1];
                    return false; // Stop iterating. We found the previous version
                }
            });

            return versionPrior; //If null, 'version' is the first version.
        },

        getSelectedVersions : function(historyDiv) {
            var selectedPageVersions = [];
            
             this.getAbsoluteDiffVersionSpans(historyDiv).each(function() {
                 var selectedVersion = $.trim($(this).text());
                 if (selectedVersion && /^\d+$/.test(selectedVersion))
                     selectedPageVersions.push(selectedVersion);
             });

            if (selectedPageVersions.length == 1 || (selectedPageVersions.length == 2 && selectedPageVersions[0] == selectedPageVersions[1])) {
                var versionPrior = this.getVersionPriorTo(historyDiv, selectedPageVersions[0]);
                if (versionPrior) {
                    if (selectedPageVersions.length == 1)
                        selectedPageVersions.push(versionPrior);
                    else
                        selectedPageVersions[1] = versionPrior;
                }
            }

            if (selectedPageVersions.length > 1)
                selectedPageVersions.sort(function(left, right) {
                    return parseInt(left) - parseInt(right);
                });

            return selectedPageVersions;
        },

        getAbsoluteDiffHeading : function(historyDiv) {
            return $("h2.plugin_pagehistory_absolute_diff_view_title", historyDiv);
        },

        getAbsoluteDiffDiv : function(historyDiv) {
            return $("div.plugin_pagehistory_absolute_diff_view", historyDiv);
        },

        showDiff : function(historyDiv) {
            var absoluteDiffDiv = this.getAbsoluteDiffDiv(historyDiv);
            var absoluteDiffHeading = this.getAbsoluteDiffHeading(historyDiv);

            var selectedPageVersions = historyslider.getSelectedVersions(historyDiv);
            if (selectedPageVersions.length == 2) {
                absoluteDiffDiv.html("<img src='" + historyslider.getContextPath() + "/images/icons/wait.gif'/>");

                $.ajax({
                    cache : false,
                    data : {
                        pageId : $("div.plugin_pagehistory_absolute_diff fieldset.hidden input[name='pageId']").val(),
                        selectedPageVersions : selectedPageVersions,
                        decorator: "none"
                    },
                    dataType: "html",
                    success : function(diffHtml) {
                        absoluteDiffDiv.empty();
                        historyslider.getPageDiffDiv(diffHtml).appendTo(absoluteDiffDiv);
                        absoluteDiffHeading.html(AJS.format($("input[name='i18n-viewingdifferencesof']", historyDiv).val(), selectedPageVersions[0], selectedPageVersions[1])).removeClass("hidden");
                    },
                    type : "GET",
                    url : historyslider.getContextPath() + "/pages/diffpagesbyversion.action"
                });

            } else {
                absoluteDiffDiv.empty();
                absoluteDiffHeading.addClass("hidden");
            }
        },

        initVersionDroppables : function(historyDiv) {
            this.getAbsoluteDiffVersionSpans(historyDiv).each(function() {
                var diffVersionSpan = $(this);

                diffVersionSpan.droppable({
                    accept : ".version-info",
                    tolerance : "pointer",
                    drop : function(event, ui) {
                        diffVersionSpan.html($("input[name='selectedPageVersions']", ui.draggable).val());
                        historyslider.showDiff(historyDiv);
                    }
                });

                diffVersionSpan.click(function(event) {
                    if (event.ctrlKey || event.metaKey) {
                        diffVersionSpan.html("&nbsp;");
                        historyslider.showDiff(historyDiv);
                    }
                });
            });
        },

        initVersionDraggables : function(historyDiv) {
            $(".version-info", historyDiv).draggable({
                helper: "clone",
                opacity: 0.5
            });
        },

        initAbsoluteDiff : function(historyDiv) {
            this.initVersionDraggables(historyDiv);
            this.initVersionDroppables(historyDiv);
        },

        initAdvancedOptionsToggle : function(historyDiv) {
            var advancedOptionsToggleOn = $("a.plugin_pagehistory_advanced_options_on", historyDiv);
            var advancedOptionsToggleOff = $("a.plugin_pagehistory_advanced_options_off", historyDiv);
            var advancedOptions = $("div.plugin_pagehistory_advanced_option", historyDiv);

            advancedOptionsToggleOn.click(function() {
                advancedOptionsToggleOn.addClass("hidden");
                advancedOptionsToggleOff.removeClass("hidden");
                advancedOptions.removeClass("hidden");
                return false;
            });

            advancedOptionsToggleOff.click(function() {
                advancedOptions.addClass("hidden");
                advancedOptionsToggleOff.addClass("hidden");
                advancedOptionsToggleOn.removeClass("hidden");
                return false;
            });
        },

        initHistorySlider : function(historyDiv) {
            historyslider.initViewPortIframes(historyDiv);
            historyslider.initAbsoluteDiff(historyDiv);
            historyslider.initAdvancedOptionsToggle(historyDiv);

            var versionsCount = historyslider.getPreviousVersionsCount(historyDiv);
            this.getSliderDiv(historyDiv).slider({
                max : versionsCount - 1,
                min : 0,
                step : 1,
                slide : function(event, ui) {
                    var params = historyslider.getParams(historyDiv);
                    historyslider.showVersion(historyDiv, params["contentId"][ui.value]);
                }
            });

            this.initSliderTicks(historyDiv, versionsCount - 1);
            this.getSliderDiv(historyDiv).focus();

            var contentIds = historyslider.getParams(historyDiv).contentId;
            historyslider.showVersion(historyDiv, contentIds[0]);

            $("a.ui-slider-handle", historyDiv).focus();
        }
    };

    $("div.plugin_pagehistoryslider").each(function() {
        historyslider.initHistorySlider($(this));
    });
});
