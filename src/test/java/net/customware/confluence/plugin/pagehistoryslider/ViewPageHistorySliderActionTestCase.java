package net.customware.confluence.plugin.pagehistoryslider;

import com.atlassian.confluence.core.VersionHistorySummary;
import com.atlassian.confluence.pages.AbstractPage;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.spaces.Space;
import junit.framework.TestCase;
import org.mockito.Mock;
import static org.mockito.Mockito.anyLong;
import static org.mockito.Mockito.when;
import org.mockito.MockitoAnnotations;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;

public class ViewPageHistorySliderActionTestCase extends TestCase
{
    @Mock
    private PageManager pageManager;

    private ViewPageHistorySliderAction viewPageHistorySliderAction;

    private Space testSpace;

    private Page testPage;

    @Override
    protected void setUp() throws Exception
    {
        super.setUp();
        MockitoAnnotations.initMocks(this);

        testSpace = new Space("TST");
        testPage = new Page();
        testPage.setSpace(testSpace);

        viewPageHistorySliderAction = new ViewPageHistorySliderAction();
        viewPageHistorySliderAction.setPageManager(pageManager);
    }

    public void testVersionSummariesConvertedToPages()
    {
        when(pageManager.getVersionHistorySummaries(testPage)).thenReturn(
                Arrays.asList(
                        new VersionHistorySummary(1, 2, "admin", new Date(), ""),
                        new VersionHistorySummary(2, 1, "admin", new Date(), "")
                )
        );

        when(pageManager.getAbstractPage(anyLong())).thenAnswer(
                new Answer<Page>()
                {
                    public Page answer(InvocationOnMock invocationOnMock) throws Throwable
                    {
                        Page aPage = new Page();
                        aPage.setSpace(testSpace);
                        aPage.setId((Long) invocationOnMock.getArguments()[0]);

                        return aPage;
                    }
                }
        );

        viewPageHistorySliderAction.setPage(testPage);
        Collection<AbstractPage> pageHistory = viewPageHistorySliderAction.getPreviousVersions();

        assertEquals(2, pageHistory.size());

        Iterator<AbstractPage> pageIter = pageHistory.iterator();

        AbstractPage abstractPage = pageIter.next();
        assertEquals(1L, abstractPage.getId());

        abstractPage = pageIter.next();
        assertEquals(2L, abstractPage.getId());
    }
    
}
